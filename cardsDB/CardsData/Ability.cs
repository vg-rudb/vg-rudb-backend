﻿using Newtonsoft.Json;

namespace VanguardNN.CardsDatabase.CardsData {
	public class Ability {

		[JsonIgnore]
		public int Id { get; set; }

		[JsonProperty("type")]
		public string Type { get; set; }

		[JsonProperty("text")]
		public string Text { get; set; }

		[JsonProperty("circle", NullValueHandling = NullValueHandling.Ignore)]
		public string Circle { get; set; }
		
		[JsonProperty("keyword", NullValueHandling = NullValueHandling.Ignore)]
		public string Keyword { get; set; }
		
		[JsonProperty("limitation", NullValueHandling = NullValueHandling.Ignore)]
		public string Limitation { get; set; }
	}
}