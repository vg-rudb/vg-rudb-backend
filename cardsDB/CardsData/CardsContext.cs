﻿using Microsoft.EntityFrameworkCore;

namespace VanguardNN.CardsDatabase.CardsData {
	public class CardsContext : DbContext {
		public DbSet<Card> Cards { get; set; }

		public CardsContext(DbContextOptions<CardsContext> options) : base(options) {
			Database.EnsureCreated();
		}
	}
}