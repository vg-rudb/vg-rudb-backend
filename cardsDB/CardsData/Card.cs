﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace VanguardNN.CardsDatabase.CardsData {
	public class Card {
		[JsonProperty("id")]
		public string Id { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("clan")]
		public string Clan { get; set; }

		[JsonProperty("trigger")]
		public string Trigger { get; set; }

		[JsonProperty("grade")]
		public int Grade { get; set; }

		[JsonProperty("skill")]
		public string Skill { get; set; }

		[JsonProperty("power")]
		public int Power { get; set; }

		[JsonProperty("shield")]
		public int Shield { get; set; }

		[JsonProperty("critical")]
		public int Critical { get; set; }

		[JsonProperty("nation")]
		public string Nation { get; set; }

		[JsonProperty("race")]
		public string Race { get; set; }

		[JsonProperty("gift")]
		public string Gift { get; set; }

		[JsonProperty("illustrator")]
		public string Illustrator { get; set; }

		[JsonProperty("abilities")]
		public List<Ability> Abilities { get; set; }

		[JsonProperty("flavor")]
		public string Flavor { get; set; }

		[JsonProperty("image")]
		public string Image { get; set; }
	}
}