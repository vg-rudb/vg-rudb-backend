﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VanguardNN.CardsDatabase.CardsData;

namespace VanguardNN.CardsDatabase.Controllers {
	[Route("api/[controller]")]
	[ApiController]
	public class CardsController : Controller {
		private CardsContext _db;

		public CardsController(CardsContext context) {
			_db = context;
		}
		
		[HttpGet("getCardById")]
		public async Task<ActionResult<IEnumerable<string>>> GetById(string cardId) {
			Card card = await _db.Cards.Include(x => x.Abilities)
			                     .FirstOrDefaultAsync(x => x.Id == cardId);
			
			return Json(card);
		}

		[HttpGet("getCards")]
		public async Task<ActionResult<IEnumerable<string>>> GetFiltered(string name, string clan, string trigger, 
		                                                                 string race, string nation, string gift,
		                                                                 string setId, int grade = -1, int power = -1, 
		                                                                 int critical = -1, int shield = -1) {
			var cards = await _db.Cards.Include(x => x.Abilities)
			                     .Where(x => string.IsNullOrEmpty(name) || x.Name.Contains(name))
			                     .Where(x => string.IsNullOrEmpty(clan) || x.Clan == clan)
			                     .Where(x => string.IsNullOrEmpty(trigger) || x.Trigger == trigger)
			                     .Where(x => string.IsNullOrEmpty(race) || x.Race == race)
			                     .Where(x => string.IsNullOrEmpty(nation) || x.Nation == nation)
			                     .Where(x => string.IsNullOrEmpty(gift) || x.Gift == gift)
			                     .Where(x => string.IsNullOrEmpty(setId) || x.Id.Contains((setId)))
			                     .Where(x => grade == -1 || x.Grade == grade)
			                     .Where(x => power == -1 || x.Power == power)
			                     .Where(x => critical == -1 || x.Critical == critical)
			                     .Where(x => shield == -1 || x.Shield == shield)
			                     .ToListAsync();

			return Json(cards);
		}

		[HttpPost("add")]
		public async Task<ActionResult<string>> Add([FromBody] Card card) {
			if (card == null) {
				return BadRequest();
			}

			await _db.Cards.AddAsync(card);
			_db.SaveChanges();
			
			return Ok(_db.Cards.Find(card.Id));
		}
	}
}